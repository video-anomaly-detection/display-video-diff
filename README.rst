========
Overview
========

An example package. Generated with cookiecutter-pylibrary.

Installation
============

::

    pip install display-video-diff

You can also install the in-development version with::

    pip install git+ssh://git@cee-gitlab.sandia.gov/video-anomaly-detection/display-video-diff.git@master

Documentation
=============


https://video-anomaly-detection.gitlab.io/display-video-diff


Development
===========

To run all the tests run::

    tox

Note, to combine the coverage data from all the tox environments run:

.. list-table::
    :widths: 10 90
    :stub-columns: 1

    - - Windows
      - ::

            set PYTEST_ADDOPTS=--cov-append
            tox

    - - Other
      - ::

            PYTEST_ADDOPTS=--cov-append tox
