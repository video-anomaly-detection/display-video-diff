============
Installation
============

This can also be installed by conda via the `conda-channel <_static/conda-channel/index.html>`_ if the most recent build included conda::

    conda install --name tmpEnv --channel http://video-anomaly-detection.cee-gitlab.lan/display-video-diff/_static/conda-channel display-video-diff

Because building a conda package significantly slows our build time, we might not build a conda package every time.
If you need a conda package of the latest commits and the `conda-channel <_static/conda-channel/index.html>`_ is empty, please reach out to us.

At the command line::

    pip install display-video-diff
