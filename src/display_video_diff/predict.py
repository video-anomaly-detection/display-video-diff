import os
import typing

import numpy as np

import display_video_diff.diffs.mse
from display_video_diff import view_diff


def ImageChops_on_ndarrays(distortedFrame, pristineFrame):
  from PIL import Image
  from PIL import ImageChops
  return ImageChops.difference(Image.fromarray(distortedFrame), Image.fromarray(pristineFrame))


def make_reduced_video(videoFileObj: typing.BinaryIO, path_to_video: str, frame_shape):
  """
  https://trac.ffmpeg.org/wiki/Scaling
   Sometimes you want to scale an image, but avoid upscaling it if its dimensions are too low.
   This can be done using min expressions:
  ffmpeg -i input.jpg -vf "scale='min(320,iw)':'min(240,ih)'" input_not_upscaled.png
  But do we want to? If we have too many total pixels, more than we can fit on the GPU, it doesn't matter if one direction is okay.

  http://ffmpeg.org/ffmpeg-filters.html#scale
  interactive widget in jupyter https://github.com/kkroening/ffmpeg-python/blob/master/examples/ffmpeg-numpy.ipynb
  https://raw.githubusercontent.com/kkroening/ffmpeg-python/master/doc/jupyter-demo.gif
  """
  import ffmpeg
  noExtension, extension = os.path.splitext(path_to_video)
  path_to_scaled_video = noExtension + '_' + str(frame_shape[0]) + '_' + str(frame_shape[1]) + extension
  ffmpeg.input(path_to_video).filter('scale', frame_shape[0], frame_shape[1]).output(path_to_scaled_video).overwrite_output().run()
  return path_to_scaled_video


def default_prediction_filepath(path_to_video):
  return os.path.splitext(path_to_video)[0] + '.predicted' + os.path.splitext(path_to_video)[1]


def default_comparison_filepath(path_to_video):
  """
  By default, we save as mp4, regardless of the original format, so we can display in a browser.
  """
  return os.path.splitext(path_to_video)[0] + '.comparison.mp4'


def save_comparison_video(wholeVideoPredictor: typing.Callable[[np.ndarray], np.ndarray],
                          inputFileObj: typing.BinaryIO,
                          outputFileObj: typing.BinaryIO,
                          path_to_video: str,
                          path_to_save_predicted_frames: str,
                          path_to_save_comparison_video: str,
                          predictionFileObj: typing.BinaryIO = None,
                          *args, **kwargs,
                          ):
  import imageio
  import skvideo.io
  inputVideo: np.ndarray = skvideo.io.vread(path_to_video)
  predictedFrames = wholeVideoPredictor(inputVideo, *args, **kwargs)

  skvideo.io.vwrite(path_to_save_predicted_frames, predictedFrames)
  # raise Exception(path_to_save_predicted_frames, predictedFrames.shape)
  comparisonFrames = view_diff.make_comparison_video(skvideo.io.vread(path_to_video), predictedFrames,
                                                     ImageChops_on_ndarrays, display_video_diff.diffs.mse.mse_rgb)
  # skvideo.io.vwrite(path_to_save_comparison_video, comparisonFrames)
  # For some unknown reason, the video is failing to display in a browser
  # when written with skvideo.io.vwrite but works fine with imageio.mimwrite,
  # although skvideo.io.vwrite works fine outside the browser.
  # Something to do with mp4, perhaps?
  imageio.mimwrite(path_to_save_comparison_video, comparisonFrames, fps=20)

  return comparisonFrames
